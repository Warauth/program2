package pizza;

public class PizzaFee extends DecoratedPizza {
  private DecoratedPizza top_link;
  private String fee_message;
  private double fee_value;

  public PizzaFee(DecoratedPizza pizza_component, String msg, double fee) {
    super(pizza_component);

    this.fee_message = msg;

    this.fee_value = fee;
  }

  public double pizzaCost()
  {
    return (super.pizzaCost() + fee_value);
  }

  public String toString()
  {
    String discount_string;

    discount_string = super.toString();
    discount_string += ("\n" + fee_message);

    return discount_string;
  }

  public String getImage()
  {
    return super.getImage();
  }
}
