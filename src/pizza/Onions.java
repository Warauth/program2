package pizza;

public class Onions extends DecoratedPizza
{

  private double pizza_cost = 0.79;

  public Onions(DecoratedPizza next_pizza)
  {
    super(next_pizza);
  }

  public double pizzaCost()
  {
    return super.pizzaCost() + pizza_cost;
  }

  public String getImage()
  {
    return super.getImage() + "O";
  }

  public String toString()
  {
    String topping_string;

    topping_string = super.toString();
    topping_string += "\nOnions";

    return topping_string;
  }
}
