package pizza;

import util.Keyboard;

public class PizzaBuilder {

  static Keyboard kb = Keyboard.getKeyboard();
  private CrustType crust_type;
  private CrustSize crust_size;
  private DecoratedPizza top_link;
  protected Crust crust_new;


  public PizzaBuilder() {
    crust_type = CrustType.THIN;
    crust_size = CrustSize.S;
    crust_new = new Crust(crust_size, crust_type);
    top_link = new Pizza(crust_new);
  }

  protected void buildPizza() {
    crust_new = new Crust(crust_size, crust_type);
    top_link = new Pizza(crust_new);
  }

  public boolean setSize(char try_size) {
    char check = try_size;
    check = Character.toUpperCase(try_size);
    while (!(check == 'S' || check == 'M' || check == 'L')) {

      System.out.println("What size pizza (S/M/L)? ");
      String new_try_size = kb.readString("");

      check = new_try_size.charAt(0);
      check = Character.toUpperCase(check);
    }

    if (check == 'S') {
      crust_size = CrustSize.S;
    }
    else if (check == 'M') {
      crust_size = CrustSize.M;
    }
    else if (check == 'L') {
      crust_size = CrustSize.L;
    }

    if ((CrustSize.S == crust_size || CrustSize.M == crust_size || CrustSize.L == crust_size)) {
      this.buildPizza();
    }

    return true;
  }

  public boolean setCrust(String try_crust) {
    try_crust = try_crust.toUpperCase();

    while (!(try_crust.equals("THIN") || try_crust.equals("HAND") || try_crust.equals("PAN")))
    {
      System.out.println("What type of crust (thin/hand/pan)? ");

      try_crust = kb.readString("");
      try_crust = try_crust.toUpperCase();
    }
    if (try_crust.equals("THIN")) {
      crust_type = CrustType.THIN;
    }
    else if (try_crust.equals("HAND")) {
      crust_type = CrustType.HAND;
    }
    else if (try_crust.equals("PAN")) {
      crust_type = CrustType.PAN;
    }

    if ((crust_type == CrustType.THIN || crust_type == CrustType.HAND || crust_type == CrustType.PAN)) {
      this.buildPizza();
    }

    return true;
  }

  public void addTopping(char topping_char) {

      if (topping_char == 'P') {
        top_link = PizzaToppingFactory.addPepperoni(top_link);
        //top_link = new Pepperoni(top_link);
      }
      else if (topping_char == 'S') {
        top_link = PizzaToppingFactory.addSausage(top_link);
        //top_link = new Sausage(top_link);
      }
      else if (topping_char == 'O') {
        top_link = PizzaToppingFactory.addOnions(top_link);
        //top_link = new Onions(top_link);
      }
      else if (topping_char == 'G') {
        top_link = PizzaToppingFactory.addGreenPeppers(top_link);
        //top_link = new GreenPeppers(top_link);
      }
      else if (topping_char == 'M') {
        top_link = PizzaToppingFactory.addMushrooms(top_link);
        //top_link = new Mushrooms(top_link);
      }
      else if (topping_char == 'H') {
        top_link = PizzaToppingFactory.addHam(top_link);
        //top_link = new Ham(top_link);
      }
      else if (topping_char == 'A') {
        top_link = PizzaToppingFactory.addPineapple(top_link);
        //top_link = new Pineapple(top_link);
      }

  }

  public DecoratedPizza pizzaDone() {
    return top_link;
  }

  public void addDiscount()
  {
    if (top_link instanceof PizzaDiscount || top_link instanceof PizzaTopping || top_link instanceof Pizza) {
      top_link = new PizzaDiscount(top_link, "senior discount", 0.10);
    }
  }
}
