package pizza;

public class Ham extends DecoratedPizza
{
  private double pizza_cost = 0.89;

  public Ham(DecoratedPizza next_pizza)
  {
    super(next_pizza);
  }

  public double pizzaCost()
  {
    return super.pizzaCost() + pizza_cost;
  }

  public String getImage()
  {
    return super.getImage() + "M";
  }

  public String toString()
  {
    String topping_string;

    topping_string = super.toString();
    topping_string += "\nHam";

    return topping_string;
  }
}
