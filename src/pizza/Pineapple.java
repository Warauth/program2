package pizza;

public class Pineapple extends DecoratedPizza
{
  private double pizza_cost = 0.79;

  public Pineapple(DecoratedPizza next_pizza)
  {
    super(next_pizza);
  }

  public double pizzaCost()
  {
    return super.pizzaCost() + pizza_cost;
  }

  public String getImage()
  {
    return super.getImage() + "A";
  }

  public String toString()
  {
    String topping_string;

    topping_string = super.toString();
    topping_string += "\nPineapple";

    return topping_string;
  }
}
