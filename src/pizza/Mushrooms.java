package pizza;

public class Mushrooms extends DecoratedPizza
{
  private double pizza_cost = 0.79;

  public Mushrooms(DecoratedPizza next_pizza)
  {
    super(next_pizza);
  }

  public double pizzaCost()
  {
    return super.pizzaCost() + pizza_cost;
  }

  public String getImage()
  {
    return super.getImage() + "M";
  }

  public String toString()
  {
    String topping_string;

    topping_string = super.toString();
    topping_string += "\nMushrooms";

    return topping_string;
  }
}
