package pizza;

public class Pizza extends DecoratedPizza {
  private Crust crust;

  public Pizza(Crust crust) {
    super();
    this.crust = crust;
  }

  public double pizzaCost() {
    return crust.crustCost();
  }

  public String getImage() {

    String image_file_string = "";
    image_file_string += crust.getSize();

    return image_file_string;
  }

  public String toString()
  {
    return (crust.toString() + "\nYour toppings:\n");
  }
}
