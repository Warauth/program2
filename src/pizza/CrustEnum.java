package pizza;

enum CrustSize
{
	S(5.99),
	M(7.99),
	L(9.99);

	private double size_cost;

	CrustSize(double sz_cost)	{
		size_cost = sz_cost;
	}

	public double getSizeCost()	{
		return size_cost;
	}
};

enum CrustType
{
  THIN(0.00),
  HAND(0.50),
  PAN(1.00);

  private double type_cost;

  CrustType(double typ_cost)
  {
    type_cost = typ_cost;
  }

  public double getTypeCost()
  {
    return type_cost;
  }

};
