package pizza;

public class VeggieLovers extends PizzaBuilder {
  private DecoratedPizza vp;
  private CrustSize crust_size;
  private CrustType crust_type;

  VeggieLovers() {
    super();
  }

  public void buildPizza() {
    VeggieLovers vl_pizza = new VeggieLovers();
    super.addTopping('G');
    super.addTopping('M');
    super.addTopping('O');
  }
}
