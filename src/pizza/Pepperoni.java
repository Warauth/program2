package pizza;

public class Pepperoni extends DecoratedPizza
{

  private double pizza_cost = 0.99;

  public Pepperoni(DecoratedPizza next_pizza)
  {
    super(next_pizza);
  }

  public double pizzaCost()
  {
    return super.pizzaCost() + pizza_cost;
  }

  public String getImage()
  {
    return super.getImage() + "P";
  }

  public String toString()
  {
    String topping_string;

    topping_string = super.toString();
    topping_string += "\nPepperoni";

    return topping_string;
  }
}
