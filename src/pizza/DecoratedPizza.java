package pizza;

public abstract class DecoratedPizza {
  private DecoratedPizza next_pizza_item;
  private double cost;

  DecoratedPizza() {
    next_pizza_item = null;
    cost = 0;
  }

  public DecoratedPizza(DecoratedPizza next_pizza) {
    this.next_pizza_item = next_pizza;
  }

  public double pizzaCost() {
    return next_pizza_item.pizzaCost();
  }

  public String toString() {
    return next_pizza_item.toString();
  }

  public String getImage() {
    return next_pizza_item.getImage();
  }
}
