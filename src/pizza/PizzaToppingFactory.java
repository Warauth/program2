package pizza;

public class PizzaToppingFactory {

  public static DecoratedPizza addPepperoni(DecoratedPizza dec_pizza) {

    PizzaTopping pepperoni = new PizzaTopping(dec_pizza, "Pepperoni", "P", 0.99);

    return pepperoni;
  }

  public static DecoratedPizza addGreenPeppers(DecoratedPizza dec_pizza) {

    PizzaTopping green_peppers = new PizzaTopping(dec_pizza, "Green Peppers", "G", 0.69);

    return green_peppers;
  }

  public static DecoratedPizza addHam(DecoratedPizza dec_pizza) {

    PizzaTopping ham = new PizzaTopping(dec_pizza, "Ham", "H", 0.89);

    return ham;
  }

  public static DecoratedPizza addMushrooms(DecoratedPizza dec_pizza) {

    PizzaTopping mushrooms = new PizzaTopping(dec_pizza, "Mushrooms", "M", 0.79);

    return mushrooms;
  }

  public static DecoratedPizza addOnions(DecoratedPizza dec_pizza) {

    PizzaTopping onions = new PizzaTopping(dec_pizza, "Onions", "O", 0.79);

    return onions;
  }

  public static DecoratedPizza addPineapple(DecoratedPizza dec_pizza) {

    PizzaTopping pineapple = new PizzaTopping(dec_pizza, "Pineapple", "A", 0.79);

    return pineapple;
  }

  public static DecoratedPizza addSausage(DecoratedPizza dec_pizza) {

    PizzaTopping sausage = new PizzaTopping(dec_pizza, "Sausage", "S", 0.99);

    return sausage;
  }
}
