package pizza;

public class GreenPeppers extends DecoratedPizza
{

  private double pizza_cost = 0.69;

  public GreenPeppers(DecoratedPizza next_pizza)
  {
    super(next_pizza);
  }

  public double pizzaCost()
  {
    return super.pizzaCost() + pizza_cost;
  }

  public String getImage()
  {
    return super.getImage() + "G";
  }

  public String toString()
  {
    String topping_string;

    topping_string = super.toString();
    topping_string += "\nGreen peppers";

    return topping_string;
  }
}
