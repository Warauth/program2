package pizza;

public class Crust
{
  private CrustSize crust_size;
  private CrustType crust_type;

  Crust(CrustSize size, CrustType type)
  {
    this.crust_size = size;
    this.crust_type = type;
  }

  public double crustCost()
  {
    return crust_size.getSizeCost() + crust_type.getTypeCost();
  }

  public String getCrust()
  {
    return crust_type.toString();
  }

  public char getSize()
  {
    String c_size = crust_size.toString();
    char sz = c_size.charAt(0);
    return sz;
  }
    
  public String toString()
  {
    return "Size: " + crust_size.toString() + "\nCrust Type: " + crust_type.toString();
  }
}
