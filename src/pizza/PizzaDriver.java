package pizza;

import java.text.DecimalFormat;
import util.Keyboard;

public class PizzaDriver
{
  static Keyboard kb = Keyboard.getKeyboard();
  static DecimalFormat df = new DecimalFormat("'$'0.00");

  public static void main(String[] args)
  {
    //DecimalFormat df = new DecimalFormat("'$'0.00");
    DecoratedPizza pf;
    PizzaBuilder pizza_builder;
    int pizza_count = 0;
    double total_order_cost = 0.0;
    boolean order_pizza = true;

    System.out.println("Would you like to order a pizza (y/n)? ");
    String order = kb.readString("");

    if (order.equals("y")) {

      while (order_pizza) {
        int pizza_choice = menu();
        if (pizza_choice == 1) {
          pizza_builder = new MeatLovers();
        }
        else if (pizza_choice == 2) {
          pizza_builder = new VeggieLovers();
        }
        else if (pizza_choice == 3) {
          pizza_builder = new Hawaiian();
        }
        else {
          pizza_builder = new PizzaBuilder();
        }

        requestSize(pizza_builder);
        requestCrust(pizza_builder);

        if (pizza_choice == 4) {
          requestToppings(pizza_builder);
        }

        pizza_count++;

        System.out.println("Are you a senior citizen (y/n)? ");
        String senior_discount = kb.readString("");
        if (senior_discount.equals("y")) {
          pizza_builder.addDiscount();
        }

        pf = pizza_builder.pizzaDone();
        System.out.println("Would you like delivery with this pizza (y/n)?");
        String delivery_fee = kb.readString("");
        if (delivery_fee.equals("y")) {
          pf = new PizzaFee(pizza_builder.pizzaDone(), "delivery", 2.50);
        }
        total_order_cost += pf.pizzaCost();

        showOrder(pf);

        System.out.println("\nWould you like to order another pizza (y/n)? ");
        order = kb.readString("");
        if (order.equals("n"))
          order_pizza = false;
      }
    }

    System.out.println("You ordered " + pizza_count + " pizza(s) for a grand total of " + df.format(total_order_cost) + ".");

  }

  //show the menu choices, wait for and return the valid selection
  private static int menu() {
    System.out.println("1. Meat Lover's \n2. Veggie Lover's \n3. Hawaiian \n4. Build Your Own \n\nSelect from the above: ");
    String choice = kb.readString("");
    int choice_int = Integer.parseInt(choice);

    while (!(choice_int == 1 || choice_int == 2 || choice_int == 3 || choice_int == 4)) {
      System.out.println("1. Meat Lover's \n2. Veggie Lover's \n3. Hawaiian \n4. Build Your Own \n\nSelect from the above: ");
      choice = kb.readString("");
      choice_int = Integer.parseInt(choice);
    }

    return choice_int;
  }

  //request the crust size, wait for a valid response confirmation from PizzaBuilder
  private static void requestSize(PizzaBuilder pizza_builder) {

    System.out.println("What size pizza (S/M/L)? ");
    String size_input = kb.readString("");
    char size_input_char = size_input.charAt(0);

    pizza_builder.setSize(size_input_char);
  }

  //request the crust type, wait for a valid response confirmation from PizzaBuilder
  private static void requestCrust(PizzaBuilder pizza_builder) {
    System.out.println("What type of crust (thin/hand/pan)? ");
    String crust_input = kb.readString("");

    pizza_builder.setCrust(crust_input);
  }

  //ask for toppings until Done indicated (invalid toppings are ignored)
  private static void requestToppings(PizzaBuilder pizza_builder) {
    System.out.println("(P)epperoni,(O)nions,(G)reen Peppers,(S)ausage,(M)ushrooms,(D)one ");
    String topping_choice = kb.readString("");

    char topping_choice_char = topping_choice.charAt(0);
    topping_choice_char = Character.toUpperCase(topping_choice_char);

    while (!(topping_choice_char == 'D')) {

      pizza_builder.addTopping(topping_choice_char);

      System.out.println("(P)epperoni,(O)nions,(G)reen Peppers,(S)ausage,(M)ushrooms,(D)one ");
      topping_choice = kb.readString("");
      topping_choice_char = topping_choice.charAt(0);
      topping_choice_char = Character.toUpperCase(topping_choice_char);

    }
  }

  //display the pizza and its total cost
  private static void showOrder(DecoratedPizza dec_pizza) {
    System.out.println("Your pizza:\n" + dec_pizza.toString() + "\n\nYour cost for the pizza is: " + df.format(dec_pizza.pizzaCost()));
  }

}
