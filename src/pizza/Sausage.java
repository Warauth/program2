package pizza;

public class Sausage extends DecoratedPizza
{

  private double pizza_cost = 0.99;

  public Sausage(DecoratedPizza next_pizza)
  {
    super(next_pizza);
  }

  public double pizzaCost()
  {
    return super.pizzaCost() + pizza_cost;
  }

  public String getImage()
  {
    return super.getImage() + "S";
  }

  public String toString()
  {
    String topping_string;

    topping_string = super.toString();
    topping_string += "\nSausage";

    return topping_string;
  }
}
