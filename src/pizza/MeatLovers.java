package pizza;

public class MeatLovers extends PizzaBuilder {
  //private CrustSize crust_size;
  //private CrustType crust_type;

  MeatLovers() {
    super();
  }

  public void buildPizza() {
    super.buildPizza();
    super.addTopping('H');
    super.addTopping('P');
    super.addTopping('S');
  }
}
