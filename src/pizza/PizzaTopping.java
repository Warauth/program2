package pizza;

public class PizzaTopping extends DecoratedPizza {
  String topping;
  String top_letter;
  double top_cost;

  public PizzaTopping(DecoratedPizza pizza_component, String topping_string, String topping_letter, double topping_cost) {
    super(pizza_component);
    this.topping = topping_string;
    this.top_letter = topping_letter;
    this.top_cost = topping_cost;
  }

  public double pizzaCost()
  {
    return super.pizzaCost() + top_cost;
  }

  public String getImage() {
    return super.getImage() + top_letter;
  }

  public String toString() {
    String topping_string;

    topping_string = super.toString();
    topping_string += topping + "\n";

    return topping_string;

  }
}
