package pizza;

import util.Keyboard;

public class PizzaDiscount extends DecoratedPizza {
  private String discount_message;
  private double actual_discount;

  //discount is assumed to be between 0.0 and 1.0
  public PizzaDiscount(DecoratedPizza pizza_component, String msg, double discount) {
    super(pizza_component);

    this.discount_message = msg;

    this.actual_discount = 1.0 - discount;
  }

  public double pizzaCost()
  {
    return (super.pizzaCost() * actual_discount);
  }

  public String toString()
  {
    String discount_string;

    discount_string = super.toString();
    discount_string += ("\n" + discount_message);

    return discount_string;
  }

  public String getImage()
  {
    return super.getImage();
  }
}
